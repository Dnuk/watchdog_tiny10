Zewnętrzny układ Watchdog. Stosowany w celu redundancji z układem wbudowanym w mikrokontroler.

Oczekuje w pętli na zbocze narastające w celu wyzerowania licznika.

Czas zadziałania około 30s, reset układu nadzorowanego realizowany przez pull-down lini reset zabezpieczanego układu.


Język C, środowisko Atmel Studio 6.x, mikrokontroler: Attiny10

-- Wszystkie materiały udostępniane są na licencji open software/hardware do użytku niekomercyjnego

-- Uwaga - kompilacja w Atmel Studio 7+ uniemożliwi ponowne przeniesienie projektu do starszej wersji.

Może też wystąpić błąd z funkcją init();. Należy używać jej wtedy jako INLINE.

---------------------------------------------------------------------------------
Schematy, pliki PCB i dokumentacja (częściowo nieaktualna):

https://bitbucket.org/Dnuk/gondolav2-hardware-repo/overview


-------------------------------------------------------------

Kontakt do nas oraz więcej materiałów tutaj: https://www.facebook.com/DNFsystems/