/*
 * attiny_main.c
 *
 *  Created on: 12-02-2014
 *      Author: Dan
 */

#include "avr/io.h" // standardowe wej/wyj
#include "util/delay.h" // op�nienia
#include "avr/interrupt.h" // przerwania
#include "avr/wdt.h" // watchdog

volatile static uint16_t timer = 0;
volatile static uint16_t timer_sek = 0;

void stan_resetowania();
void stan_wys_imped();

void init()
{
	wdt_enable(WDTO_4S);
	wdt_reset();

	DDRB = 0b111011;
	PORTB = 0b000000;

// konfiguracja timer0 8bit
	OCR0A = 78; //Output Compare Register A >>> 8000000/1024 = 7812,5/78 = 100,16025 ~100Hz
	TCCR0A |= (1<<WGM01); // ctc
	TCCR0B |= (1<<CS02) | (1<<CS00); // presc. 1024
	TIMSK0 |= (1<<OCIE0A); // Timer/Counter0 Output Compare Match A Interrupt Enable

// konfiguracja przerwania zewnetrznego
	EICRA |= (1<<ISC00)|(1<<ISC01); // przerwanie od INT0, gdy zbocze narastaj�ce
	EIMSK |= (1<<INT0);           //  ustawia kt�re przerwania zewn. b�d� aktywne

	sei(); // wlaczenie przerwan


}

ISR(TIM0_COMPA_vect)
{
	timer++; 		// inkrementowanie zmiennych timer*

	if(timer >= 100)
	{
		timer_sek++;
	}
}

ISR(INT0_vect)
{
	timer = 0;			// zerowanie licznikow
	timer_sek = 0;
}

int main()
{
	init();

	while(1)
	{
		wdt_reset();

		if(timer_sek >= 100)
		{
			stan_resetowania();	// podciagnij stan do masy i resetuj inny uklad
			_delay_ms(150);
			stan_wys_imped();  // wroc do stanu wysokiej impedancji i pozwol ukladowi pracowac

			timer = 0;
			timer_sek = 0;
		}
		else
		{
			stan_wys_imped();	// utrzymaj stan wysokiej impedancji jesli warunek nie spelniony
		}


	}
}

void stan_resetowania()
{
	wdt_reset();

	DDRB |= _BV(1);  // jako wyjscie
	PORTB &= ~_BV(1);  // stan niski
}

void stan_wys_imped()
{
	wdt_reset();

	DDRB &= ~_BV(1); // jako wejscie
	PORTB &= ~_BV(1);  // stan wysokiej impedancji bez podciagu
}


